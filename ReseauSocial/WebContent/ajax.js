
function getXMLHttpRequest() {
	var xhr = null;
	xhr = new XMLHttpRequest(); 
    return xhr;
}

//Affichage des r�sultats d'une recherche :
function recherche(callback) {
	var xhr = getXMLHttpRequest();
	
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			callback(xhr.responseXML);
		}
	};
	
	var nomPrenom = encodeURIComponent(document.getElementById("champRecherche").value);
		
	xhr.open("GET", "Servlet1?operation=recherche&chaine="+nomPrenom, true);
	xhr.send(null);
}


function afficherResultats(result) {
	var elementInsertion = "";
    $(result).find('resultat').each(   
            function()
            {
               var nom = $(this).find('nom').text();
               var prenom = $(this).find('prenom').text();
               var id = $(this).find('id').text();
               var ami = $(this).find('ami').text();
               
               if (ami=="1") {
            	   elementInsertion += "<a class='amis'><img src='Images/wall/Avatar.jpg'/><p>"+prenom+" "+nom+"</p></a>";
               }
               else {
            	   elementInsertion += "<div class='autres'><img src='Images/wall/Avatar.jpg'/><p>"+prenom+" "+nom+"</p> <div class='menu'> <a href='Servlet1?operation=ajouterAmi&whoIsAsked="+id+"' target='_self' class='raccourci'><img rel='tooltip' src='Images/wall/Plus.png' title='Ajouter a ma liste d'amis' height='16' width='16'/></a></div></div>";
               }               
             });	
    $('#resultatsRecherche').html(elementInsertion);
}

function afficherInvitations(callback) {
	
	var xhr = getXMLHttpRequest();
	
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			callback(xhr.responseXML);
		}
	};
	xhr.open("GET", "Servlet1?operation=getInvitations", true);
	xhr.send(null);
}

function invitation(result) {
	var elementInsertion = "";
    $(result).find('invitation').each(   
            function()
            {
               var nom = $(this).find('nom').text();
               var prenom = $(this).find('prenom').text();
               var id = $(this).find('id').text();
        	   elementInsertion+= "<div class='invitation'><img src='Images/wall/Avatar.jpg'/><p>"+prenom+" "+nom+"</p><div class='menu'><a href='Servlet1?operation=reponseDemandeAmi&whoAsked="+id+"&reponse=0' target= <div class='menu'> <a target='_self' class='raccourci'><img rel='tooltip' src='Images/wall/Refuse.png' title='Refuser l'invitation' onclick='supprimerDemande(this)' height='16' width='16'/></a>" 
        	   + "<a href='Servlet1?operation=reponseDemandeAmi&whoAsked="+id+"&reponse=0' target='_self' class='raccourci'><img rel='tooltip' src='Images/wall/Accept.png' title='Accepter l'invitation' onclick='supprimerDemande(this)' height='16' width='16'/></a></div></div>";
             });		

 $("#nouvelles-invitations>div").html(elementInsertion);
}

function actualiserHome(callback) {
	
	var xhr = getXMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			callback(xhr.responseXML);
		}
	};
	xhr.open("GET", "Servlet1?operation=actualisationPosts&etat=home", true);
	xhr.send(null);
}

function actualisationHome(result) {
	var elementInsertion = "";
	var nomP = $(this).find('nomperso').text();
	var dateP = $(this).find('datepresente').text();
	$(result).find('post').each(   
			function()
			{
				var nom = $(this).find('nomduposteur').text();
				var surmur = $(this).find('surmur').text();
				if (surmur=="1")
					var nomdudestinataire = $(this).find('nomdudestinataire').text();
				var date = $(this).find('date').text();
				var texte = $(this).find('texte').text();

				elementInsertion+= "<div><div class='bloc-post'>"
					+"<div class='posteur'><a href='#'><img rel='tooltip' class='avatar' src='Images/wall/Avatar.jpg' title="+nom+"/></a>"+date+"</div>"
					+"<p class='post'>"+texte+"</p>"
					+"<div class='menu'>"
					+"<a target='_self' class='raccourci'><img rel='tooltip' src='Images/wall/Commentaires.png' onclick='afficherCommentaires(this)' title='Afficher les Commentaires' height='16' width='16'/></a>"
					+"<a target='_self' class='raccourci'><img rel='tooltip' src='Images/wall/Ecrire.png' onclick='nouveauCommentaire(this)' title='Ajouter un commentaire' height='16' width='16'/></a>"
					+"</div></div>";
				var commentaires = $(this).find('commentaires');
				commentaires.find('commentaire').each(
						function()
						{
							var nomC = $(this).find('nomducommentateur').text();
							var surmur = $(this).find('surmur').text();
							if (surmur=="1")
								var nomdudestinataire = $(this).find('nomdudestinataire').text();
							var dateC = $(this).find('dateC').text();
							var texteC = $(this).find('texteC').text();
							elementInsertion+= "<div class='bloc-commentaire'>"
								+"<div><a href='#'><img rel='tooltip' src='Images/wall/Avatar.jpg' title="+nomC+"/></a>"+dateC+"</div>"
								+"<p class='commentaire'>"+texteC+"</p></div>"
						});
			});
	elementInsertion+= "<div class='bloc-nouveau-commentaire'>"
		+"<div><a href='#'><img rel='tooltip' src='Images/wall/Avatar.jpg' title="+nomP+"/></a>"+dateP+"</div>"
		+"<form><textarea>Valeur par défaut</textarea></form>"
		+"<button class='btn btn-warning' type='submit'>Publier</button></div>"
		
		$("#mur").html(elementInsertion);
}

function actualiserProfil(callback) {
	
	var xhr = getXMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			callback(xhr.responseXML);
		}
	};
	xhr.open("GET", "Servlet1?operation=actualisationPosts&etat=profil", true);
	xhr.send(null);
}

function actualisationProfil(result) {
	var elementInsertion = "";
	var nomP = $(this).find('nomperso').text();
	var dateP = $(this).find('datepresente').text();
	$(result).find('post').each(   
			function()
			{
				var nom = $(this).find('nomduposteur').text();
				var surmur = $(this).find('surmur').text();
				if (surmur=="1")
					var nomdudestinataire = $(this).find('nomdudestinataire').text();
				var date = $(this).find('date').text();
				var texte = $(this).find('texte').text();

				elementInsertion+= "<div><div class='bloc-post'>"
					+"<div class='posteur'><a href='#'><img rel='tooltip' class='avatar' src='Images/wall/Avatar.jpg' title="+nom+"/></a>"+date+"</div>"
					+"<p class='post'>"+texte+"</p>"
					+"<div class='menu'>"
					+"<a target='_self' class='raccourci'><img rel='tooltip' src='Images/wall/Commentaires.png' onclick='afficherCommentaires(this)' title='Afficher les Commentaires' height='16' width='16'/></a>"
					+"<a target='_self' class='raccourci'><img rel='tooltip' src='Images/wall/Ecrire.png' onclick='nouveauCommentaire(this)' title='Ajouter un commentaire' height='16' width='16'/></a>"
					+"</div></div>";
				var commentaires = $(this).find('commentaires');
				commentaires.find('commentaire').each(
						function()
						{
							var nomC = $(this).find('nomducommentateur').text();
							var surmur = $(this).find('surmur').text();
							if (surmur=="1")
								var nomdudestinataire = $(this).find('nomdudestinataire').text();
							var dateC = $(this).find('dateC').text();
							var texteC = $(this).find('texteC').text();
							elementInsertion+= "<div class='bloc-commentaire'>"
								+"<div><a href='#'><img rel='tooltip' src='Images/wall/Avatar.jpg' title="+nomC+"/></a>"+dateC+"</div>"
								+"<p class='commentaire'>"+texteC+"</p></div>"
						});
			});
	elementInsertion+= "<div class='bloc-nouveau-commentaire'>"
		+"<div><a href='#'><img rel='tooltip' src='Images/wall/Avatar.jpg' title="+nomP+"/></a>"+dateP+"</div>"
		+"<form><textarea>Valeur par défaut</textarea></form>"
		+"<button class='btn btn-warning' type='submit'>Publier</button></div>"
		
		$("#mur").html(elementInsertion);
}

