<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.GlobalBean" %>
<%@ page import="model.Post" %>
<%@ page import="model.Comment" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Date"  %>
<%@ page import="java.text.DateFormat"  %>
<%@ page import="model.UserContainer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="PagePersoStyle.css"  />
<link href="dist/css/bootstrap.min.css"  rel="stylesheet" type="text/css"/>
<script src="jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="script.js" type="text/javascript"></script>
<script src="ajax.js" type="text/javascript"></script>

<title>Insert title here</title>
</head>
<body>
		<%GlobalBean gb = (GlobalBean) request.getAttribute("globalBean");
		String id = gb.get_id().toString();%>
<header>
	<div id="milieu">
		<div id="menu-principal">
			<a rel="tooltip" title="Home" class="onglet-pas-default" onclick="afficherOnglet(this); afficherPage('le-mur'); actualiserHome(actualisationHome)">
				<h1>Home</h1>
			</a>
			<a rel="tooltip" title="Profil" class="onglet-default" onclick="afficherOnglet(this); afficherPage('le-profil'); actualiserProfil(actualisationProfil)">
				<h1>Profil</h1>
			</a>
			<a rel="tooltip" title="Amis" class="onglet-default" onclick="afficherOnglet(this); afficherPage('les-amis'); afficherInvitations(invitation)">
				<h1>Amis</h1>
			</a>
			<a rel="Photo" title="Photos" class="onglet-default" onclick="afficherOnglet(this); afficherPage('les-photos')">
				<h1>Photos</h1>
			</a>							
		</div>
	</div>
</header>
    
<div id="principale">
	<div id="left">
		<div id="le-mur">
<%DateFormat shortDf = DateFormat.getDateInstance(DateFormat.SHORT); %>					
<%ArrayList<Post> myPost = (ArrayList<Post>) gb.get_postContainer();
if (myPost!=null) {
	for(int i=0; i< myPost.size();i++){%>
			<div>
				<div class="bloc-post">
					<div class="posteur"><a href="#"><img rel="tooltip" class="avatar" src="Images/wall/Avatar.jpg" title= <%= myPost.get(i).get_author() %> /></a> <%= shortDf.format(myPost.get(i).get_date()) %></div>
					<p class="post">
					<%= myPost.get(i).get_body() %>
					</p>
					<div class="menu">
						<a target="_self" class="raccourci"><img rel="tooltip" src="Images/wall/Commentaires.png" onclick="afficherCommentaires(this)" title="Afficher les Commentaires" height="16" width="16"/></a>
						<a target="_self" class="raccourci"><img rel="tooltip" src="Images/wall/Ecrire.png" onclick="nouveauCommentaire(this)" title="Ajouter un commentaire" height="16" width="16"/></a>
					</div>
				</div>	
				
				<%ArrayList<Comment> myComments = (ArrayList<Comment>) myPost.get(i).get_comments();
				if (myComments!=null) {
				for(int j=0; j< myComments.size();j++){%>
				
				<div class="bloc-commentaire">
					<div><a href="#"><img rel="tooltip" src="Images/wall/Avatar.jpg" title= <%= myComments.get(j).get_author() %> /></a> <%= shortDf.format(myComments.get(j).get_date()) %> </div>
					<p class="commentaire">
					<%= myComments.get(j).get_body() %>
					</p>
				</div>
				 <% } %>
				<% } %>
				<div class="bloc-nouveau-commentaire">
					<div><a href="#"><img rel="tooltip" src="Images/wall/Avatar.jpg" title="Paf"/></a> <%  Date d = new Date(); %> <%= shortDf.format(d) %> > </div>
					<FORM>
					<TEXTAREA name="nom">Valeur par défaut</TEXTAREA>
					</FORM>	
					<button class="btn btn-primary" type="submit">Publier</button>	
				</div>				
			</div>	
<%	} %>
<% } %>	
						
		<div class="clear"></div>																					
		</div>
		<div id="le-profil">
			<div>
				<div class="bloc-post">
					<div class="posteur"><a href="#"><img rel="tooltip" class="avatar" src="Images/wall/Avatar.jpg" title="Plop"/></a>16 Jan. 2014</div>
					<p class="post">
					Plop
					</p>
					<div class="menu">
						<a target="_self" class="raccourci"><img rel="tooltip" src="Images/wall/Commentaires.png" onclick="afficherCommentaires(this)" title="Afficher les Commentaires" height="16" width="16"/></a>
						<a target="_self" class="raccourci"><img rel="tooltip" src="Images/wall/Ecrire.png" onclick="nouveauCommentaire(this)" title="Ajouter un commentaire" height="16" width="16"/></a>
					</div>
				</div>
				<div class="bloc-nouveau-commentaire">
					<div><a href="#"><img rel="tooltip" src="Images/wall/Avatar.jpg" title="Paf"/></a>18 Jan. 2014</div>
					<FORM>
					<TEXTAREA name="nom">Valeur par défaut</TEXTAREA>
					</FORM>	
					<button class="btn btn-warning" type="submit">Publier</button>	
				</div>						
			</div>
			<div class="clear"></div>	
		</div>
		<div id="les-amis">	
			<div id="nouvelles-invitations"> 
				<h2>Nouvelles invitations</h2>	
				<div>			
				</div>		
			</div>
			<div id="recherche-personne">
				<h2>Recherche</h2>
				<div>
					<input type="text" id="champRecherche"> <button class="btn btn-warning" onclick="recherche(afficherResultats);" type="submit">Rechercher</button>
				</div>
				<div id="resultatsRecherche">
				</div>
			</div>
		</div>
		<div id="les-photos">
			<h4 style="text-align:center; margin-top:20px">Section en chantier !<h4/>
		</div>

	</div>
	<div class="clear"></div>	
	
	<div id="right">
		<div id="profil">
			<div id="utilisateur">
				<a href="?operation=voirMonMur&id=<%=gb.get_id().toString() %>" target="_self"><img src="Images/wall/Avatar.jpg" title="<%=gb.get_surname() %>" id="avatar" height="80" width="80"/></a>
				<p> <%= gb.get_surname() %> <br/> <%= gb.get_name() %> </p>	
				<div id="raccourci-profil">
					<a href="Servlet1?operation=deconnexion" target="_self" class="raccourci"><img rel="tooltip" src="Images/wall/Deconnexion.png" title="Déconnexion" height="16" width="16"/></a>	
				</div>							
			</div>
			<div id="amis">
				<h2>Amis</h2>
				<div id="liste-amis">	
					<% List<UserContainer> amis = gb.get_myFiendsContainer();%>
					<% for (int i = 0 ; (i < 9) && (i < amis.size()); i++) { %>
						<a href="Servlet1?operation=voirAmi&id=<%=amis.get(i).get_id().toString()%>"><img rel="tooltip" src="Images/wall/Avatar.jpg" title="<%= amis.get(i).get_surname() %>" class="aminonconnecte"/></a>
					<% } %>
					<div id="menu-amis">
						<a target="_self" class="raccourci"><img rel="tooltip" src="Images/wall/ListeComplete.png" onclick="script:afficherListeComplete('liste-amis-complete')" title="Liste complète" height="16" width="16"/></a>
					</div>	
				</div>
				<div id="liste-amis-complete" >
					<% for(int i = 0 ; i < amis.size(); i++) { %>
						<a href="Servlet1?operation=voirAmi&id=<%=amis.get(i).get_id().toString()%>"><img src="Images/wall/Avatar.jpg" class="aminonconnecte"/><p><%= amis.get(i).get_surname() %></p></a>
					<% } %>					
				</div>				
			</div>
			<form method="post" action="Servlet1" class= "newPost">

					<div id="posts-rapides">
						
						<h2>New Post</h2>
						<FORM>
							<TEXTAREA name="body" id="nouveau-post">Valeur par défaut</TEXTAREA>
							<input type="hidden" name="authorId" value = "<%= gb.get_id().toString() %>" >
							<input type="hidden" name="author" value = "<%= gb.get_login() %>" >
							<input type="hidden" name="wallId" value = "<%=  gb.get_currentWallId().toString() %>" >
							<input type="hidden" name="walType" value = "<%= gb.get_currentWallType() %>" >
			
						</FORM>
				
						<div id="publier">
							<button class="btn btn-warning" type="submit" name="operation"
								value="publication">Publier</button>
						</div>
					</div>
				</form>
			<div id="home">
				<a href="#" target="_self"><img rel="tooltip" src="Images/wall/Home.png" title="Home" id="image-home" height="64" width="64"/></a>					
			</div>						
		</div>
		<div class="clear"></div>	
	</div>	
	<div class="clear"></div>		
</div>
	
<footer>
</footer>
</body>
</html>