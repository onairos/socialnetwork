package servlets;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.jms.Session;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bson.types.ObjectId;

import test.CompletionBase;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;

import controler.EJBGestionPost;
import controler.EJBGestionUser;
import controler.EJBPrincipal;
import controler.dao.UserDAO;
import model.GlobalBean;
import model.Post;
//import model.PostJSP;
import model.User;
//import model.UserJSP;

public class Servlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	CompletionBase cmplB;
	EJBPrincipal ejbP;
	Datastore datastore;
	GlobalBean gbb;
	HttpSession session;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    public void init(ServletConfig config){
	System.out.println("On passe dans la m�thode init de Servlet1");
	
 	Morphia morphia = new Morphia();
	morphia.mapPackage("model");
	MongoClientOptions.Builder builder = new MongoClientOptions.Builder().alwaysUseMBeans(true);
	try {
		datastore= morphia.createDatastore(	new MongoClient("localhost", builder.build()),"intergiciel");
	} catch (UnknownHostException e) {
		e.printStackTrace();
	}
	
	ejbP = new EJBPrincipal(datastore);
	
    }
	
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		
		System.out.println("op =");
		if (request.getParameter("operation").toString().equals("recherche")) {

			if (request.getParameter("chaine") == null) {
			} else {
				String[] nomPrenom = request.getParameter("chaine").split("\\s");

				String chaine1 = nomPrenom[0];
				String chaine2 = null;
				if (nomPrenom.length > 1) {
					chaine2 = nomPrenom[1];
				}
				String res = new String();
			
			
				System.out.println("\n"+ "Operation = recherche" + chaine1 + chaine2+ "\n");
				
				System.out.println(gbb.get_myFriends().toString());
				
				if (chaine1 == null && chaine2 != null) {
					res = ejbP.USERListToAjax(ejbP.UserfindByNameOrSurname(chaine2),gbb.get_myFriends());
//					ArrayList<User> userL = (ArrayList<User>) ejbP.UserfindByNameOrSurname(chaine2);
//					System.out.println(userL);
				} else if (chaine1 != null && chaine2 == null) {
					res = ejbP.USERListToAjax(ejbP.UserfindByNameOrSurname(chaine1),gbb.get_myFriends());
//					ArrayList<User> userL = (ArrayList<User>) ejbP.UserfindByNameOrSurname(chaine1);
//					System.out.println(userL);
				} else if (chaine1 != null && chaine2 != null) {
					//res = ejbP.USERListToAjax(ejbP.USERfindByNameAndSurnameOrSurnameAndName(chaine1,chaine2), gbb.get_myFriends());
				
				}
				if(res!=null){
					System.out.println("\n res = "+res);
				}else{
					System.out.println("\n res est null");
				}
//				res = "<resultats><resultat><nom>Isaac</nom><prenom>Louis</prenom><ami>0</ami></resultat><resultat><nom>JM</nom><prenom>DouxDoux</prenom><ami>0</ami></resultat></resultats>";
				
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.getWriter().write(res);
			}
		} else if ((request.getParameter("operation")).equals("getInvitations")) {
			System.out.println("On passe dans operation == getInvitations !!! \n\n");
			
			ObjectId objId = (ObjectId) session.getAttribute("_id");
			User usr = ejbP.USERfindById(objId);
			ArrayList<User> demandeAmis = new ArrayList<User>();
			for (ObjectId amiPotentiel : usr.get_waitingForYourFriendshipResponse()) {
				demandeAmis.add(ejbP.USERfindById(amiPotentiel));
			}
			String res = ejbP.FRIENSHIPAccueilAjax(demandeAmis);
			
			System.out.println(res);
			
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(res);
		}
		else if ((request.getParameter("operation")).equals("demandeAmi")) {
			String whoAsked = request.getParameter("whoAsked");
			ObjectId whoAskedId = new ObjectId(whoAsked);
			ObjectId whoWasAskedId = (ObjectId) session.getAttribute("_id");
			ejbP.USERaddAskForFrienship(whoAskedId, whoWasAskedId);
			
		} else if ((request.getParameter("operation")).equals("reponseDemandeAmi")) {
			ObjectId whoAsked = new ObjectId(request.getParameter("whoAsked"));
			ObjectId whoAccepted = (ObjectId) session.getAttribute("_id");
			int etat = Integer.parseInt(request.getParameter("reponse"));
			if (etat == 1)
				ejbP.USERaddFriend(whoAccepted, whoAsked);
			else
				ejbP.USERrefuseFriend(whoAccepted, whoAsked);
		} 
		else if(request.getParameter("operation").equals("ajouterAmi")) {
			ObjectId  myId = (ObjectId) session.getAttribute("_id");
			ObjectId whoIsAsked = new ObjectId(request.getParameter("whoIsAsked"));
			ejbP.USERaddAskForFrienship  (myId, whoIsAsked);
			
			
		} else if (request.getParameter("operation").equals("actualisationPosts")) {
		
			ObjectId id = gbb.get_currentWallId();
			List<Post> liste = null;
			if (id.equals(gbb.get_id())) {
				String op = request.getParameter("etat");
				if (op.equals("home")) {
					User user = ejbP.USERfindById(id);
					liste = ejbP.POSTfindByAuthorIdsDateDscWithLimit(user.get_myFriends(), 20);
				} else {
					ArrayList<ObjectId> p = new ArrayList<ObjectId>();
					p.add(id);
					liste = ejbP.POSTfindByAuthorIdsDateDscWithLimit(p, 20);
				}
			} else {
				ArrayList<ObjectId> p = new ArrayList<ObjectId>();
				p.add(id);
				liste = ejbP.POSTfindByAuthorIdsDateDscWithLimit(p, 20);
			}
			String res = ejbP.POSTAccueilAjax(liste);
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(res);
		} else if (request.getParameter("operation").equals("deconnexion")){
			session.invalidate();
			gbb = new GlobalBean();
			request.getRequestDispatcher("accueil.html").forward(request, response);
			
		}
		
		
		

	}
		
		

	
	
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		
//	 	Morphia morphia = new Morphia();
//		morphia.mapPackage("model");
//		//morphia.mapPackageFromClass(User.class);
//		MongoClientOptions.Builder builder = new MongoClientOptions.Builder().alwaysUseMBeans(true);
//		try {
//			datastore= morphia.createDatastore(	new MongoClient("localhost", builder.build()),"intergiciel");
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//		ejbP = new EJBPrincipal(datastore);
	    
		
		// Savoir quelle op�ration on traite
		String operation = request.getParameter("operation");
		
		if (operation.equals("inscription")) {
			// R�cup�rer les informations du futur User
			String name = request.getParameter("name");
			String surname = request.getParameter("surname");
			String email = request.getParameter("email");
			String password = request.getParameter("password");


			
		gbb = ejbP.registerUser(name, surname, email, password);
		/* AJOUTER LES ATTRIBUTS A LA REPONSE */
		/* REDIRIGER */
		if(gbb==null){	
			request.getRequestDispatcher("accueil.html").forward(request, response);
		}
		else{
		session = request.getSession(true);
		session.setAttribute("_id", gbb.get_id());
		
		request.setAttribute("globalBean", gbb);
		request.getRequestDispatcher("wall.jsp").forward(request, response);
		}

		}
		
		else if (operation.equals("login")){
			
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			
			gbb = ejbP.login(email, password);
			if(gbb!= null){
				
				System.out.println("into login");
				
			session = request.getSession(true);
			session.setAttribute("_id", gbb.get_id());
				
			request.setAttribute("globalBean", gbb);
			request.getRequestDispatcher("wall.jsp").forward(request, response);
			}
			else{
				request.getRequestDispatcher("accueil.html").forward(request, response);
			}
			
			/* AJOUTER LES ATTRIBUTS A LA REPONSE */
			/* REDIRIGER */
			
		}
		
		else if (operation.equals("publication")) {
			
			String body = request.getParameter("body");
			String authorId = (String) request.getParameter("authorId");
			String author = (String) request.getParameter("author");
			String wallId = (String) request.getParameter("wallId");
			String wallType = (String) request.getParameter("wallType");
			
			System.out.println(body +"\n"+ authorId +"\n"+ author +"\n"+ wallId +"\n"+ "_user");
			
			Date date = new Date();
			ejbP.POSTcreatePost(authorId, author, body, wallType, wallId, date);
			Post post = ejbP.POSTfindByDateAndAuthor(date, authorId);
			System.out.println(post.toString());
			ejbP.USERaddPostInvolvedIn(authorId, post.get_authorId().toString());
		}

	

		
	}

}

