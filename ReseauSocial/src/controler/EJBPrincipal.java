package controler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.bson.types.ObjectId;

import com.google.code.morphia.Datastore;

import controler.dao.PostDAO;
import controler.dao.UserDAO;
import controler.dao.dao_impl.PostDAO_impl;
import controler.dao.dao_impl.UserDAO_impl;
import model.Comment;
import model.GlobalBean;
import model.Group;
import model.GroupContainer;
import model.Picture;
import model.Post;
import model.User;
import model.UserContainer;

//@Stateless
public class EJBPrincipal implements  EJBPrincipal_itf{
	
	private UserDAO userDAO;
	private PostDAO postDAO;
	private GlobalBean gb;
	
	public EJBPrincipal(Datastore datastore){
		userDAO = new UserDAO_impl(datastore);
		postDAO = new PostDAO_impl(datastore);
	}
	
	
	
	
//create un global bean sans completer picturesContainer ni groupContainer ( pour l instant en tout cas !!! )
	
	public GlobalBean createGlobalBean( User user){
		GlobalBean gb= new GlobalBean();
		gb.set_id ( user.get_id());
		gb.set_surname ( user.get_surname());
		gb.set_name ( user.get_name());
		gb.set_email ( user.get_email()); 
		gb.set_login (user.get_email());
		gb.set_postInvolved ( user.get_postInvolved());
		gb.set_myFriends ( user.get_myFriends());
		gb.set_currentWallType("_user");
		
		if(user.get_myFriends()!=null && ! user.get_myFriends().isEmpty())
		gb.set_myFiendsContainer( (ArrayList<UserContainer>) UserContainer.toList(this.USERfindUserByIds(user.get_myFriends() )));
		
		if(user.get_myPictures()!=null)
		gb.set_Pictures(user.get_myPictures());
		//gb.set_myPicturesContainer(			);
		
		gb.set_Groups(user.get_myGroups());
		//gb.set_myGroupContainer();
		
		if(user.get_waitingForYourFriendshipResponse()!=null && ! user.get_waitingForYourFriendshipResponse().isEmpty()){
		gb.set_waitingForYourFriendshipResponse(user.get_waitingForYourFriendshipResponse());
		gb.set_waitingForYourFriendshipResponseContainer( (ArrayList<UserContainer>) UserContainer.toList(this.USERfindUserByIds( user.get_waitingForYourFriendshipResponse() ))   );
		}
		
		if(user.get_responsesYouAreWaitingFor()!=null && ! user.get_responsesYouAreWaitingFor().isEmpty())
		gb.set_responsesYouAreWaitingFor(user.get_responsesYouAreWaitingFor());
		
		System.out.println(user.get_waitingForYourFriendshipResponse());
		
		if(user.get_responsesYouAreWaitingFor()!=null && ! user.get_myFriends().isEmpty())
		gb.set_responsesYouAreWaitingForContainer( (ArrayList<UserContainer>) UserContainer.toList(this.USERfindUserByIds(user.get_responsesYouAreWaitingFor())));
		
		
		
		return gb;
	}
	
	
	
//	public GlobalBean updateName(ObjectId id, String name){
//		// mettre à jour l'objet GlobalBean
//		//avant de propager la requête en base
//	}
//public GlobalBean updateSurname(ObjectId id){
//		
//	}
//public GlobalBean updateEmail(ObjectId id){
//	
//}
//public GlobalBean updateLogin(ObjectId id){
//	
//}
//public GlobalBean updatePostInvolved(ObjectId id){
//	
//}
//public GlobalBean updateFriends(ObjectId id){
//	
//}
//public GlobalBean updatePictures(ObjectId id){
//	
//}
//public GlobalBean updateGroups(ObjectId id){
//	
//}
//public GlobalBean updateWaitingForYourFriendshipResponse(ObjectId id){
//	
//}
//public GlobalBean updateResponsesYouAreWaitingFor(ObjectId id){
//	
//}
	
	
	
	
	
	
	
	/*********************************************************/
	/*														 */
	/*														 */	
	/*							USER						 */
	/*														 */
	/*														 */
	/*														 */
	/*********************************************************/
	
	
	public GlobalBean registerUser(String name,String surname, String email, String password){
		if (!this.USERemailPasswordValid(email, password)) {
			this.createUser(name, surname, email, password);
			
			System.out.print(" \n \n ");
			System.out.print("l'email n'existait pas on a crée l'utilisateur sans erreur");
			System.out.print(" \n \n ");

			List<User> lUser = (ArrayList<User>) this.USERfindByEmail(email);
			
			if (lUser != null) {
				System.out
						.print("recherche user par email passe sans erreur");
				

				if (lUser.isEmpty()) {
					System.out
							.println("la recherche par email a renvoyé une liste vide \n ");
				} else {
					
					User user1 = lUser.get(0);

					this.gb = this.createGlobalBean(user1);
					
					/////////////////////////////////////////////////	
					List<Post> myFriendsPost = new ArrayList<Post>();
					myFriendsPost = this.POSTfindByAuthorIdsDateDscWithLimit(this.gb.get_myFriends(), 20);
					this.gb.set_postContainer((ArrayList<Post>)myFriendsPost);
					this.gb.set_currentWallId(user1.get_id());
					this.gb.set_currentWallType("_user");
					
					/////////////////////////////////////////////////		

				}

			} else {
				System.out.print("recherche user par email erreur");
			}
		} else {
			System.out.print(" \n l 'utilisateur existe deja ");
		}
		return this.gb;
	}
	
	
	

	
	
	
	public GlobalBean login(String email, String password){
		
		if (this.USERemailPasswordValid(email, password)) {
			
			List<User> lUser = (ArrayList<User>) this.USERfindByEmail(email);
			
			if (lUser != null) {
				System.out
						.print("recherche user par email passe sans erreur");
				

				if (lUser.isEmpty()) {
					System.out
							.println("la recherche par email a renvoyé une liste vide \n ");
					return null;
				} else {
					
					User user1 = lUser.get(0);
//					System.out
//					.println(user1.toString());
					
					
					/**************************************/
					/*                                    */
					/*        SESSION A CREER ?           */
					/**************************************/
					
					
					
					this.gb = this.createGlobalBean(user1);
					
					/////////////////////////////////////////////////
					
					List<Post> myFriendsPost = new ArrayList<Post>();
					myFriendsPost = this.POSTfindByAuthorIdsDateDscWithLimit(this.gb.get_myFriends(), 20);
					
					if( myFriendsPost!=null && ! myFriendsPost.isEmpty()){
					this.gb.set_postContainer((ArrayList<Post>)myFriendsPost);
					}
					
					this.gb.set_currentWallId(user1.get_id());
					this.gb.set_currentWallType("_user");
					
					///////////////////////////////////////////////
					
//					System.out
//					.println(gbb.toString());
					
					System.out.println( "\n\n");
					return this.gb;
				}

			} else {
				System.out.print("recherche user par email erreur");
				return null;
			}
			
		}else{
			return null;
			//rediriger ou informer la page d'accueil que l 'utilisateur n 'existe pas
		}
		
	}
	
	
	/**********************************************/
	/**********************************************/
	/**********************************************/
	/**********************************************/
	
	public void createUser(String name, String surname, String email, String password){
		User user = new User();
		user.set_name(name);
		user.set_surname(surname);
		user.set_email(email);
		user.set_password(password);
		userDAO.create(user);
	}
	
	public void createUser(User user){
		userDAO.create(user);
	}
	/**********************************************/
	public  List<User> USERfindUserByIds( List<ObjectId> ids){
		if( ids == null){
			return new ArrayList<User>();
		}
		else{
			return userDAO.findByIds(ids);
		}
	}
	
//	public List<User> USERfindUserByIds( List<String> ids){
//		List<ObjectId> ids1 = new ArrayList <ObjectId>();
//		for(String id : ids){
//			ids1.add(new ObjectId(id));
//		}
//		return userDAO.findByIds(ids1);
//	}
	
	/**********************************************/
	
	public User USERfindById( ObjectId id){
		if(id==null){
			return null;
		}
		else{
		return userDAO.findById(id);
		}
	}
	public User USERfindById( String id){
		if(id==null){
			return null;
		}
		else{
		return userDAO.findById(id);
		}
	}
	
	/**********************************************/
	
	public List<User> USERfindByEmail(String email){
		if(email==null){
			return null;
		}
		else{
		return userDAO.findByEmail(email);
		}
	}
	
	/**********************************************/
	
	// Ne fonctionne pas actuellement
	
	public List<User> USERfindByNameAndSurnameOrSurnameAndName(String chaine1,
			String chaine2){
		return userDAO.findByNameAndSurnameOrSurnameAndName(chaine1, chaine2);
	}
	
	/**********************************************/
	
	public List<User> UserfindByNameOrSurname(String chaine1){
		if(chaine1==null){
			return null;
		}
		else{
		return userDAO.findByNameOrSurname(chaine1);
		}
	}
	
	/**********************************************/
	
	public String USERListToAjax(List<User> users, List<ObjectId> friends) {
		String rep = "<resultats>";
		for (User usr : users) {
		
			rep+="<resultat>";
			rep+="<prenom>" + usr.get_surname() + "</prenom>";
			rep+="<nom>" + usr.get_name() + "</nom>";
			rep+="<id>"+usr.get_id()+"</id>";
			if (friends.contains(usr.get_id()))
				
				rep+="<ami>" + "1" + "</ami>";
			else
				rep+="<ami>" + "0" + "</ami>";
			rep+="</resultat>";
		}
		rep+="</resultats>";
		
		return rep;
	}
	
	/**********************************************/
	public boolean USERareFiends(ObjectId objId, ObjectId friendId){
		if(objId==null || friendId==null){
			return false;
		}else{
		return userDAO.areFiends(objId, friendId);
		}
	}
	
	public boolean USERareFiends(String objId, ObjectId friendId){
		if(objId==null || friendId==null){
			return false;
		}else{
		ObjectId objId1 = new ObjectId( objId);
		return userDAO.areFiends(objId1, friendId);
		}
	}
	
	/**********************************************/
	public boolean USERemailPasswordValid(String email, String password){
		if(email==null || password==null){
			return false;
		}else{
		return userDAO.emailPasswordValid(email, password);
		}
	}
	
	
	/**********************************************/
	/* UPDATE NON UTILISES                        */
	/**********************************************/
	
	
	public void USERupdateSNELP(ObjectId userId, String surname, String name,
			String email, String login, String password){
		userDAO.updateSNELP(userId, surname, name, email, login, password);
	}
	
	public void USERupdateSNELP(String userId, String surname, String name,
			String email, String login, String password){
		ObjectId userId1 = new ObjectId(userId);
		userDAO.updateSNELP(userId1, surname, name, email, login, password);
	}
	
	
	/**********************************************/
	public void USERupdateWholeUser(String _id, User user){
		userDAO.updateWholeUser(_id, user);
	}
	
	public void USERupdateWholeUser(ObjectId _id, User user){
		userDAO.updateWholeUser(_id, user);
	}
	/**********************************************/
	
	public void USERaddAskForFrienship(ObjectId whoAsked, ObjectId whoWasAsked) {
		if(whoAsked!=null || whoWasAsked!=null){
			userDAO.addAskForFrienship(whoAsked, whoWasAsked);
		}
	}

	public void USERaddFriend(ObjectId whoAccepted, ObjectId whoAsked){
		if(whoAsked!=null || whoAsked!=null){
			userDAO.addFriend(whoAccepted, whoAsked);
		}
	}
	
	public void USERaddFriend(String whoAccepted, ObjectId whoAsked){
		if(whoAsked!=null || whoAsked!=null){
		ObjectId whoAccepted1 = new ObjectId(whoAccepted);
		userDAO.addFriend(whoAccepted1, whoAsked);
		}
	}
	
	/**********************************************/
	public void USERdeleteFriend(ObjectId user1, ObjectId user2){
		if(user1!=null || user2!=null){
		userDAO.deleteFriend(user1, user2);
		}
	}
	
	public void USERdeleteFriend(String user1, String user2){
		if(user1!=null || user2!=null){
		ObjectId user11 = new ObjectId(user1);
		ObjectId user21 = new ObjectId(user2);
		userDAO.deleteFriend(user11, user21);
		}
	}
	/**********************************************/
	public void USERrefuseFriend(ObjectId whoRefused, ObjectId whoAsked){
		if(whoRefused!=null || whoAsked!=null){
		userDAO.refuseFriend(whoRefused, whoAsked);
		}
	}
	
	public void USERrefuseFriend(String whoRefused, String whoAsked){
		if(whoRefused!=null || whoAsked!=null){
		ObjectId whoRefused1 = new ObjectId(whoRefused);
		ObjectId whoAsked1 = new ObjectId(whoAsked);
		userDAO.refuseFriend(whoRefused1, whoAsked1);
		}
	}
	
	/**********************************************/
	public void USERaddPostInvolvedIn(ObjectId userId, ObjectId postId){
		if(userId!=null || postId!=null){
		userDAO.addPostInvolvedIn(userId, postId);
		}
	}
	
	public void USERaddPostInvolvedIn(String userId, String postId){
		if(userId!=null || postId!=null){
		ObjectId userId1 = new ObjectId(userId);
		ObjectId postId1 = new ObjectId(postId);
		userDAO.addPostInvolvedIn(userId1, postId1);
		}
	}
	
	/**********************************************/
	public void USERdeletePostInvolvedIn(ObjectId userId, ObjectId postId){
		if(userId!=null || postId!=null){
		userDAO.deletePostInvolvedIn(userId, postId);
		}
	}
	
	public void USERdeletePostInvolvedIn(String userId, String postId){
		if(userId!=null || postId!=null){
		ObjectId userId1 = new ObjectId(userId);
		ObjectId postId1 = new ObjectId(postId);
		userDAO.deletePostInvolvedIn(userId1, postId1);
		}
	}
	
	/**********************************************/
	public void USERaddGroupToUser(ObjectId userId, ObjectId groupId){
		if(userId!=null || groupId!=null){
		userDAO.addGroupToUser(userId, groupId);
		}
	}
	
	public void USERaddGroupToUser(String userId, String groupId){
		if(userId!=null || groupId!=null){
		ObjectId userId1 = new ObjectId(userId);
		ObjectId groupId1 = new ObjectId(groupId);
		userDAO.addGroupToUser(userId1, groupId1);
		}
	}
	
	
	/**********************************************/
	public void USERdeleteGroupFromUser(ObjectId userId, ObjectId groupId){
		if(userId!=null || groupId!=null){
		userDAO.deleteGroupFromUser(userId, groupId);
		}
	}
	
	public void USERdeleteGroupFromUser(String userId, String groupId){
		if(userId!=null || groupId!=null){
		ObjectId userId1 = new ObjectId(userId);
		ObjectId groupId1 = new ObjectId(groupId);
		
		userDAO.deleteGroupFromUser(userId1, groupId1);
		}
	}
	
	/**********************************************/
	public void USERaddPictureToUser(ObjectId userId, ObjectId pictureId){
		if(userId!=null || pictureId!=null){
		userDAO.addPictureToUser(userId, pictureId);
		}
	}
	
	public void USERaddPictureToUser(String userId, String pictureId){
		if(userId!=null || pictureId!=null){
		ObjectId userId1 = new ObjectId(userId);
		ObjectId pictureId1 = new ObjectId(pictureId);
		userDAO.addPictureToUser(userId1, pictureId1);
		}
	}
	
	/**********************************************/
	
	public void USERdeletePictureFromUser(ObjectId userId, ObjectId pictureId){
		if(userId!=null || pictureId!=null){
		
		userDAO.deletePictureFromUser(userId, pictureId);
		}
	}
	
public void USERdeletePictureFromUser(String userId, String pictureId){
	if(userId!=null || pictureId!=null){
	ObjectId userId1 = new ObjectId(userId);
	ObjectId pictureId1 = new ObjectId(pictureId);
		
		userDAO.deletePictureFromUser(userId1, pictureId1);
		}
	}
	
	/**********************************************/
	public void USERdelete(String id){
		if(id!=null ){
		userDAO.delete(id);
		}
	}
	public void USERdelete(ObjectId id){
		if(id!=null ){
		userDAO.delete(id);
		}
	}
	
	
	
	/*********************************************************/
	/*														 */
	/*														 */	
	/*					POST & COMMENT						 */
	/*														 */
	/*														 */
	/*														 */
	/*********************************************************/
	
	
public void POSTcreatePost( String authorId, String author, String body, String postedOnType, String postedOnId){
	if(authorId!=null && author!=null && body!=null && postedOnType !=null &&postedOnId!=null){
	Post post = new Post();
	post.set_authorId(new ObjectId(authorId));
	post.set_author(author);
	post.set_body(body);
	post.set_date(new Date());
	post.set_postedOnId(new ObjectId(postedOnId));
	post.set_postedOnType(postedOnType);
	postDAO.create(post);
	}
	
}

public void POSTcreatePost(String authorId, String author, String body, String postedOnType, String postedOnId, Date d) {
	if(authorId!=null && author!=null && body!=null && postedOnType !=null &&postedOnId!=null){
	Post post = new Post();
	post.set_authorId(new ObjectId(authorId));
	post.set_author(author);
	post.set_body(body);
	post.set_date(d);
	post.set_postedOnId(new ObjectId(postedOnId));
	post.set_postedOnType(postedOnType);
	postDAO.create(post);
	}
}


public List<Post> POSTfindPostedOnDateDsc(String wallEntityType, ObjectId id){
	if(wallEntityType!=null && id!=null ){
	return postDAO.findPostedOnDateDsc(wallEntityType, id);
	}else{
		return null;
	}
}

public List<Post> POSTfindPostedOnDateDscWithLimit(String wallEntityType, ObjectId id, int limit){
	if(wallEntityType!=null && id!=null && limit!=0){
	return postDAO.findPostedOnDateDscWithLimit(wallEntityType, id, limit);
	}else{
		return null;
	}
}


public List<Post> POSTfindByAuthorIdsDateDscWithLimit(List<ObjectId> _authorIds, int limit){
	if(_authorIds!=null && limit!=0){
	return postDAO.findByAuthorIdsDateDscWithLimit(_authorIds, limit);
	}else{
		return null;
	}
	
}

public Post POSTfindByDateAndAuthor(Date d, String authorId) {
	if(d!=null && authorId!=null ){
	return postDAO.findByDateAndAuthor(d, authorId);
	}else{
		return null;
	}
}


public void POSTupdateWholePost(String _id, Post post){
	if(_id!=null && post!=null ){
	postDAO.updateWholePost(_id, post);
	}
}

	public void POSTupdateWholePost(ObjectId _id, Post post) {
		if(_id!=null && post!=null ){
		postDAO.updateWholePost(_id, post);
		}
	}

public void POSTaddComment(ObjectId postId, Comment comment){
	if(postId!=null && comment!=null ){
	postDAO.addComment(postId, comment);
	}
}

public void POSTdeleteComment ( ObjectId postId, Comment comment){
	if(postId!=null && comment!=null ){
	postDAO.deleteComment(postId, comment);
	}
}


public void POSTdelete(String id) {
	if(id!=null  ){
	postDAO.delete(id);
	}
}
public void POSTdelete(ObjectId id){
	if(id!=null  ){
	postDAO.delete(id);
	}
}
public void POSTdelete(Post post){
	if(post!=null  ){
	postDAO.delete(post);
	}
}

public String POSTAccueilAjax(List<Post> posts) {
	
	String reponse = "<posts>";
	reponse += "<nomperso>" + gb.get_surname() + " " + gb.get_name() + "</nomperso>";
	reponse += "<datepresente>" + (new Date()).toString() + "</datepresente>";
	if(posts!=null  ){
	for (Post p : posts) {
		reponse+="<post>";
		reponse+="<nomduposteur>" + p.get_author() + "</nomduposteur>";
		if (p.get_authorId().equals(p.get_postedOnId())){
			reponse+="<surmur>0</surmur>";
		} else {
			reponse+="<surmur>1</surmur>";
			User destinataire = userDAO.findById(p.get_postedOnId());
			reponse+="<nomdudestinataire>" + destinataire.get_surname() + " " + destinataire.get_name() + "</nomdudestinataire>";
		}
		reponse+= "<date>" + p.get_date().toString() + "</date>";
		reponse+="<texte>" + p.get_body() + "</texte>";
		String commentaires = COMMENTAccueilAjax(p.get_comments());
		reponse+=commentaires;
		reponse+="</post>";
	}
	reponse += "<posts>";
	}
	return reponse;
}

public String COMMENTAccueilAjax(List<Comment> comments) {
	String reponse= "";
	reponse += "<commentaires>";
	if(comments!=null  ){
		for (Comment c :comments) {
			reponse+="<commentaire>";
			reponse+="<nomducommentateur>" + c.get_author() + "</nomducommentateur>";
			reponse+="<texteC>" + c.get_body() +"</texteC>";
			reponse+="<dateC>" + c.get_date().toString() + "</dateC>";
			reponse+="</commentaire>";
		}
	}
	reponse+="</commentaires>";
	return reponse;
}

public String FRIENSHIPAccueilAjax(List<User> potentialFriends) {
	String reponse = "<invitations>";
	if(potentialFriends!=null  ){
	for (User usr : potentialFriends) {
		reponse += "<invitation>";
		reponse += "<nom>";
		reponse += usr.get_name();
		reponse += "</nom>";
		reponse += "<prenom>";
		reponse += usr.get_surname();
		reponse += "</prenom>";
		reponse += "<id>";
		reponse += usr.get_id().toString();
		reponse += "</id>";
		reponse += "</invitation>";
	}
	reponse += "</invitations>";
	}
	return reponse;
}



@Override
public void createPicture() {
	// TODO Auto-generated method stub
	
}



@Override
public void createGroup() {
	// TODO Auto-generated method stub
	
}



@Override
public void createEvent() {
	// TODO Auto-generated method stub
	
}



}
